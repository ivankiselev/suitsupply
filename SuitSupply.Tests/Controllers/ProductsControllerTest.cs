﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SuitsupplyClassLibrary;
using System.Web.Http;
using System.Web.Http.Description;
using System.Net;
using System.Net.Http;
using System.Web.Http.Results;
using SuitSupplyREST.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Entity;
namespace SuitSupplyREST.Tests.Controllers
{
    //class for mocking
    public class MockProductsContext : IProductContainer
    {
        public List<Product> Products; 
        public MockProductsContext()
        {
            Products = new List<Product>();
            Products.Add(new Product { Id = 1, Name = "Position1", Price = 234, LastUpdated = DateTime.Now });
            Products.Add(new Product { Id = 2, Name = "Position2", Price = 235, LastUpdated = DateTime.Now });
            Products.Add(new Product { Id = 3, Name = "Position3", Price = 236, LastUpdated = DateTime.Now });
        }
        public IQueryable<Product> GetAllProductsSync()
        { 
            var queryable = Products.AsQueryable();
            return queryable;
        }
        public async Task<List<Product>> GetAllProducts()
        {
            return  this.Products;
        }
        public Product GetProductWithNoCache(int id)
        {
            return this.Products.Where(x => x.Id == id).First();
        }
        public async Task<Product> GetProduct(int? id)
        {
            try
            {
                return this.Products.Find(x => x.Id == id);
            }
            catch (Exception err)
            {
                return null;
            }
        }
        public async Task<bool> InsertProduct(Product curr)
        {
            if (curr.Id != 0 || curr.Name != null || curr.Price > 0)
            {
                Products.Add(curr);
            }
            return true;
        }
        public async Task<bool> DeleteProduct(int id)
        {
            try
            {
                Products.Remove(Products.First(x => x.Id == id));
            }
            catch (Exception err)
            {
            }
            return true;
        }
        public async Task<bool> EditProduct(Product curr)
        {
            try
            {
                Products.Remove(Products.First(x => x.Id == curr.Id));
                Products.Add(curr);
            }
            catch (Exception err)
            {
            }
            return true;
        }
       public  void Dispose()
        { }
    }
    //classes for mocking
    public class MockLogging : ILogWriter
    {
        public void WriteMessage(string Message)
        {
        }
        public void WriteError(Exception Error)
        {
        }
    }

    [TestClass]
    public class ProductControllerTest
    {
        [TestMethod]
        public void GetViewOfProducts()
        {
            MockProductsContext MCC = new MockProductsContext();
            MockLogging MCLG = new MockLogging();
            ProductsController PC = new ProductsController(MCC, MCLG);
            var result = PC.GetProducts();
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void GetViewOfProduct()
        {
            MockProductsContext MCC = new MockProductsContext();
            MockLogging MCLG = new MockLogging();
            ProductsController PC = new ProductsController(MCC, MCLG);
            var result = PC.GetProduct(2);
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void InsertProduct()
        {
            MockProductsContext MCC = new MockProductsContext();
            MockLogging MCLG = new MockLogging();
            ProductsController PC = new ProductsController(MCC, MCLG);
             PC.PostProduct(new Product { Id=3,Name="sdf", Price=234});

            var result = PC.GetProducts();
            Assert.AreEqual(result.Count(),4);
        }
        [TestMethod]
        public void DeleteProduct()
        {
            MockProductsContext MCC = new MockProductsContext();
            MockLogging MCLG = new MockLogging();
            ProductsController PC = new ProductsController(MCC, MCLG);
            var result = PC.DeleteProduct(2);
            Assert.IsNotNull(result);

            var result2 = PC.GetProduct(2);

            Assert.IsInstanceOfType(result2.Result, typeof(NotFoundResult));
        }
        [TestMethod]
        public void EditProduct()
        {
            MockProductsContext MCC = new MockProductsContext();
            MockLogging MCLG = new MockLogging();
            ProductsController PC = new ProductsController(MCC, MCLG);
            var result = PC.PutProduct(1, new Product {Id= 1, Name= "Changed", Price= 234 });
            Assert.IsNotNull(result); 
        }
        [TestMethod]
        public void GetViewOfInvalidProduct()
        {
            MockProductsContext MCC = new MockProductsContext();
            MockLogging MCLG = new MockLogging();
            ProductsController PC = new ProductsController(MCC, MCLG);
            var result = PC.GetProduct(66);
            Assert.IsInstanceOfType(result.Result, typeof(NotFoundResult));
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void InsertProductWithMissingFields()
        {
            MockProductsContext MCC = new MockProductsContext();
            MockLogging MCLG = new MockLogging();
            ProductsController PC = new ProductsController(MCC, MCLG);
            var result = PC.PostProduct(new Product());
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void DeleteProductWithWrongID()
        {
            MockProductsContext MCC = new MockProductsContext();
            MockLogging MCLG = new MockLogging();
            ProductsController PC = new ProductsController(MCC, MCLG);
            var result = PC.DeleteProduct(277);
            Assert.IsInstanceOfType(result.Result, typeof(NotFoundResult));
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void EditNonExistingProduct()
        {
            MockProductsContext MCC = new MockProductsContext();
            MockLogging MCLG = new MockLogging();
            ProductsController PC = new ProductsController(MCC, MCLG);
            var result = PC.PutProduct(2341,new Product());
            Assert.IsInstanceOfType(result.Result, typeof(BadRequestResult));
            Assert.IsNotNull(result);
        }

    }
}
