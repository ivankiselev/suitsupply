﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using System.Web;
namespace SuitsupplyClassLibrary
{
    //lets just save message with ID, without exact changes.
   public class LogWriter: ILogWriter
    {
        public void WriteMessage(string Message)
        {   
            using (StreamWriter sw = File.AppendText(HttpContext.Current.Server.MapPath("~/App_Data/") + ConfigurationManager.AppSettings["LogFileName"]))
            { 
                    sw.WriteLine(DateTime.Now.ToString()+" " + Message+Environment.NewLine);
            }
        }
        public void WriteError(Exception Error)
        {
            using (StreamWriter sw = File.AppendText(HttpContext.Current.Server.MapPath("~/App_Data/") + ConfigurationManager.AppSettings["ErrorFileName"]))
            {
                sw.WriteLine(DateTime.Now.ToString() + " " + Error.Message + Environment.NewLine);
            }
        }

    }
    //interface for mock
  public interface ILogWriter
    {
          void WriteMessage(string Message);
          void WriteError(Exception Error);
    }
}
