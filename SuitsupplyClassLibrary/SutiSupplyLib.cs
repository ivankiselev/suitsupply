﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
namespace SuitsupplyClassLibrary
{ 
    //interface for mocking for unit tests
    public interface IProductContainer
    { 
        Task<List<Product>> GetAllProducts();
        IQueryable<Product> GetAllProductsSync();
        Task<Product> GetProduct(int? id);
        Task<bool> InsertProduct(Product prod);
        Task<bool> DeleteProduct(int id);
        Task<bool> EditProduct(Product curr);
        Product GetProductWithNoCache(int id);
        void Dispose();
    }

    //implementating of interface to help with mocking
    public partial class ProductContainer : IProductContainer
    {
        public IQueryable<Product> GetAllProductsSync()
        {
            return this.Products;
        }
        public Task<List<Product>> GetAllProducts()
        {
            return this.Products.ToListAsync();
        }
        public Task<Product> GetProduct(int? id)
        {
            return this.Products.FindAsync(id);
        }
        public Product GetProductWithNoCache(int id)
        {
            return this.Products.AsNoTracking().Where(x => x.Id == id).First();
        }
        public async Task<bool> InsertProduct(Product prod)
        {
            this.Products.Add(prod);
            await this.SaveChangesAsync();
            return true;
        }
        public async Task<bool> DeleteProduct(int id)
        {
            Product product = await this.GetProduct(id);
            this.Products.Remove(product);
            await this.SaveChangesAsync();
            return true;
        }
        public async Task<bool> EditProduct(Product prod)
        {
            this.Entry(prod).State = EntityState.Modified;
            await this.SaveChangesAsync();
            return true;
        }
    }


    /// <summary>
    /// Data Annotations for Product
    /// </summary>
    [MetadataType(typeof(ProductMD))]
    public partial class Product
    {
    }
    internal sealed class ProductMD
    {
        [Range(0, Double.MaxValue)]
        public double Price;
    }
}
