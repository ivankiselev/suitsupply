﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SuitsupplyClassLibrary;
using System.Web.Http.Filters; 

namespace SuitSupplyREST.Controllers
{ 
    //class for logging errors
    public class NotImplExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            LogWriter lg = new LogWriter();
            lg.WriteError(context.Exception);
            //not sure if we should change message
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                Content = new StringContent(context.Exception.Message),
                ReasonPhrase = "Exception"
            });
        }
    }
    [NotImplExceptionFilterAttribute]
    public class ProductsController : ApiController
    {
        // Interfaces for mocking
        IProductContainer db;
        ILogWriter lg;
        public ProductsController()
        {
            db = new ProductContainer();
            lg = new LogWriter();
        }
        public ProductsController(IProductContainer context, ILogWriter LgWr)
        {
            db = context;
            lg = LgWr;
        }
         
        // GET: api/Products
        // get all products
        public IQueryable<Product> GetProducts()
        { 
                return db.GetAllProductsSync();
        }

        // GET: api/Products/5
        // get exact product
        [ResponseType(typeof(Product))]
        public async Task<IHttpActionResult> GetProduct(int id)
        {
            Product product = await db.GetProduct(id);
            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        // PUT: api/Products/5
        // modify product
        [ResponseType(typeof(void))]
        //just in case
        [System.Web.Http.HttpPut]
        public async Task<IHttpActionResult> PutProduct(int id, Product product)
        {
            product.LastUpdated = DateTime.Now;
            if (!ModelState.IsValid || (product.Photo != null && !IsImage(product.Photo)))
            {
                return BadRequest(ModelState);
            }
            if (id != product.Id)
            {
                return BadRequest();
            }
            try
            {
                await db.EditProduct(product);
                lg.WriteMessage(String.Format("Product with ID {0} is edited", product.Id)); 
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Products 
        // insert product
        [ResponseType(typeof(Product))]
        public async Task<IHttpActionResult> PostProduct(Product product)
        {
            product.LastUpdated = DateTime.Now;
            if (!ModelState.IsValid || (product.Photo != null && !IsImage(product.Photo)))
            {
                return BadRequest(ModelState);
            }
           

            await db.InsertProduct(product);
            lg.WriteMessage(String.Format("Product with ID {0} is created", product.Id));
            return CreatedAtRoute("DefaultApi", new { id = product.Id }, product);
        }

        // DELETE: api/Products/5 
        // delete product
        [ResponseType(typeof(Product))]
        public async Task<IHttpActionResult> DeleteProduct(int id)
        {
            Product product = await db.GetProduct(id);
            if (product == null)
            {
                return NotFound();
            }

            await db.DeleteProduct(id);
            lg.WriteMessage(String.Format("Product with ID {0} is deleted", id));

            return Ok(product);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //check if product exists
        private bool ProductExists(int id)
        {
            return db.GetProduct(id)!=null;
        }

        /// <summary>
        /// determine if inputed array is image by known signatures
        /// we could put it in data annotations, but we further we can put some another extensions, so let it be here
        /// </summary>
        /// <param name="Array"></param>
        /// <returns></returns>
        private bool IsImage(byte[] Array)
        {

            List<string> jpg = new List<string> { "FF", "D8" };
            List<string> png = new List<string> { "89", "50", "4E", "47", "0D", "0A", "1A", "0A" };
            List<List<string>> imgTypes = new List<List<string>> { jpg, png };

            List<string> bytesIterated = new List<string>();

            for (int i = 0; i < 8; i++)
            {
                string bit = Array[i].ToString("X2");
                bytesIterated.Add(bit);

                bool isImage = imgTypes.Any(img => !img.Except(bytesIterated).Any());
                if (isImage)
                {
                    return true;
                }
            }

            return false;
        }
    }
}