﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SuitsupplyClassLibrary;

namespace SuitSupplyREST.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        [HandleError]
        public ViewResult Index()
        { 
            return View("Error");
        }
        public ViewResult NotFound()
        {
            Response.StatusCode = 404;  //you may want to set this to 200
            return View("NotFound");
        }
    }
}