﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.IO;
using System.Web.Mvc;
using SuitsupplyClassLibrary;

namespace SuitSupplyREST.Controllers
{

    [HandleError(ExceptionType = typeof(Exception), Master = "Error", View = "Index")]
    public class ProductsWebPageController : Controller
    {
        // Interfaces
        IProductContainer db;
        ILogWriter lg;
        public ProductsWebPageController()
        {
            db = new ProductContainer();
            lg = new LogWriter();
        }
        public ProductsWebPageController(IProductContainer context, ILogWriter LgWr)
        {
            db = context;
            lg = LgWr;
        }
        // GET: ProductsWebPage
        public async Task<ActionResult> Index()
        {
            return View(await db.GetAllProducts());
        }

        // GET: ProductsWebPage/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = await db.GetProduct(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: ProductsWebPage/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProductsWebPage/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Price")] Product product, HttpPostedFileBase file)
        {
            product.LastUpdated = DateTime.Now;
            if (ModelState.IsValid)
            {
                product.Photo = GetPhotoOrBytes(file, product.Id);

                await db.InsertProduct(product);
                lg.WriteMessage(String.Format("Product with ID {0} is created", product.Id));
                return RedirectToAction("Index");
            }

            return View(product);
        }

        // GET: ProductsWebPage/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = await db.GetProduct(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: ProductsWebPage/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Price,LastUpdated")] Product product, HttpPostedFileBase file)
        {
            product.LastUpdated = DateTime.Now;
            if (ModelState.IsValid)
            {
                product.Photo = GetPhotoOrBytes(file, product.Id);
                await db.EditProduct(product);
                lg.WriteMessage(String.Format("Product with ID {0} is edited", product.Id));
                return RedirectToAction("Index");
            }
            return View(product);
        }

        // GET: ProductsWebPage/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = await db.GetProduct(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: ProductsWebPage/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            await db.DeleteProduct(id);
            lg.WriteMessage(String.Format("Product with ID {0} is deleted", id));
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //override onexception method
        protected override void OnException(ExceptionContext filterContext)
        {
            Exception exception = filterContext.Exception;
            //Logging the Exception
            lg.WriteError(exception);
            filterContext.ExceptionHandled = true;
            
            var Result = this.View("Error", new HandleErrorInfo(exception,
               filterContext.RouteData.Values["controller"].ToString(),
                filterContext.RouteData.Values["action"].ToString()));
            filterContext.Result = Result;

        }

        /// <summary>
        /// render image based on exisitng bytes
        /// no async operations 
        /// </summary>
        /// <param name="id"> id of product</param>
        /// <returns></returns>
        public ActionResult RenderImage(int id)
        {
            return File(GetBytes(id), "image/png");
        }
        /// <summary>
        /// Get Bytes of existing Photo by id
        /// </summary>
        /// <param name="id"> id of product</param>
        /// <returns></returns>
        private byte[] GetBytes(int id)
        {
            Product prod = db.GetProductWithNoCache(id);
            return prod.Photo;
        }
        /// <summary>
        /// Get Bytes of existing Photo or push existed bytes to model
        /// no async methods here - otherwise EF can clinch about data state
        /// </summary>
        /// <param name="file"> control from view</param>
        /// <param name="id"> id of product</param>
        /// <returns></returns>
        private byte[] GetPhotoOrBytes(HttpPostedFileBase file, int id)
        {
            if (file != null && file.ContentLength > 0)
            {
                byte[] FileArray;
                using (var reader = new System.IO.BinaryReader(file.InputStream))
                {
                    FileArray = reader.ReadBytes(file.ContentLength);
                }
                return FileArray;
            }
            else if (id != 0 && db.GetProductWithNoCache(id) != null)
            {
                return GetBytes(id);
            }
            else { return null; }
        }
    }
}
